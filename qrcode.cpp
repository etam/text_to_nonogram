#include <cerrno>
#include <cstring>
#include <stdexcept>

#include "qrcode.hpp"

#include "color.hpp"


namespace etam {
namespace qrcode {


Image::Image(::QRcode* qrcode)
    : mSelf(qrcode, &::QRcode_free)
{}

std::size_t Image::width() const
{
    return mSelf->width;
}

std::size_t Image::height() const
{
    return mSelf->width; // it's square
}

Color Image::at(std::size_t y, std::size_t x) const
{
    return mSelf->data[y*width() + x] & 1 ? Color::Black : Color::White;
}

Image encodeString(const std::string& string, const int version, const ::QRecLevel revLevel)
{
    const auto image = ::QRcode_encodeString(string.c_str(), version, revLevel, ::QR_MODE_8, 1);
    if (!image) {
        const auto errno_bak = errno;
        using namespace std::string_literals;
        throw Error{"Creation of image failed: "s + strerror(errno_bak)};
    }
    return {image};
}

std::vector<Image> encodeStringAllVariants(const std::string& string)
{
    auto result = std::vector<Image>{};
    
    for (int version = 1; version <= 7/*QRSPEC_VERSION_MAX*/; ++version) {
        for (int recLevel = ::QR_ECLEVEL_L; recLevel <= ::QR_ECLEVEL_H; ++recLevel) {
            try {
                result.push_back(encodeString(string, version, static_cast<QRecLevel>(recLevel)));
            } catch (Error&) {}
        }
    }
    
    return result;
}

namespace {

__attribute__((destructor)) void clearCache()
{
    ::QRcode_clearCache();
}

} // namespace


} // namespace qrcode
} // namespace etam
