#ifndef ETAM_LATEX_HPP
#define ETAM_LATEX_HPP

#include <ostream>
#include <vector>


namespace etam {

class Nonogram;


template <typename T>
struct _ToLatex
{
    const T& v;
};

template <typename T>
inline
_ToLatex<T> toLatex(const T& v)
{
    return {v};
}

std::ostream& operator<<(std::ostream& ostream, _ToLatex<std::vector<Nonogram>> _toLatex);


} // namespace etam

#endif // ETAM_LATEX_HPP
