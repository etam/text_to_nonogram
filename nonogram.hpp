#ifndef ETAM_NONOGRAM_HPP
#define ETAM_NONOGRAM_HPP

#include <vector>

#include "image.hpp"


namespace etam {

namespace pbnsolve {
enum class Result;
} // namespace pbnsolve


class Nonogram
{
public:
    typedef std::vector<std::vector<std::size_t>> Hints;

private:
    Image mGoal;
    Hints mRowHints;    // [from top to bottom][from left to right]
    Hints mColumnHints; // [from left to right][from top to bottom]
    pbnsolve::Result mSolvability;

public:
    explicit
    Nonogram(Image image);

    const Image& goal() const;
    const Hints& rowHints() const;
    const Hints& columnHints() const;
    pbnsolve::Result solvability() const;

    bool isSolvable() const;
};


} // namespace etam

#endif // ETAM_NONOGRAM_HPP
