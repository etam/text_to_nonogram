CC = gcc
CXX = g++
#CPPFLAGS =
# FIXME: segfaults with enabled optimizations
CXXFLAGS = -fdiagnostics-color=auto -std=c++14 -O0 -g -Wall -Wextra -Wno-unused-local-typedefs
#LDFLAGS = 
LDLIBS = -lstdc++ -ldmtx -lPocoFoundation -lqrencode
OBJS = dmtx.o image.o latex.o main.o nonogram.o pbnsolve.o qrcode.o
EXE = main


all: $(EXE)
$(EXE): $(OBJS)


DEPS = $(OBJS:.o=.d)

$(DEPS): %.d: %.cpp
	@set -e; rm -f $@; \
	 DIR=`dirname $<`; \
	 $(CXX) -MM $(CXXFLAGS) $(CPPFLAGS) $< | \
	 sed "s,^\(.*\)\.o:,$$DIR/\1.d $$DIR/\1.o:," > $@

ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif


clean:
	$(RM) $(EXE) $(OBJS) $(DEPS)

.PHONY: all clean
