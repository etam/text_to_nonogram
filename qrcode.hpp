#ifndef ETAM_QRCODE_HPP
#define ETAM_QRCODE_HPP

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <qrencode.h>


namespace etam {

enum class Color;


namespace qrcode {


class Error
    : public std::runtime_error
{
private:
    typedef std::runtime_error ParentType;
public:
    using ParentType::ParentType;
};


class Image
{
private:
    std::unique_ptr<::QRcode, void(*)(::QRcode*)> mSelf;

public:
    Image(::QRcode* qrcode);

    std::size_t width() const;
    std::size_t height() const;
    Color at(std::size_t y, std::size_t x) const;
};


Image encodeString(const std::string& string, int version = 0, ::QRecLevel revLevel = ::QR_ECLEVEL_L);
std::vector<Image> encodeStringAllVariants(const std::string& string);


} // namespace qrcode
} // namespace etam

#endif // ETAM_QRCODE_HPP
