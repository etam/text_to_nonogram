#ifndef ETAM_IMAGE_HPP
#define ETAM_IMAGE_HPP

#include <array>

#include <boost/multi_array.hpp>

#include <etam/slicing_iterator.hpp>

#include "color.hpp"


namespace etam {


class Image
{
public:
    typedef boost::multi_array<Color, 2> DataType;
    typedef SlicingIterator<Color,2,0> RowsSlicingIterator;
    typedef ConstSlicingIterator<Color,2,0> ConstRowsSlicingIterator;
    typedef SlicingIterator<Color,2,1> ColumnsSlicingIterator;
    typedef ConstSlicingIterator<Color,2,1> ConstColumnsSlicingIterator;

private:
    DataType mData;

    Image(DataType data);

public:
    template <typename CImage>
    static
    Image fromCImage(const CImage& cImage)
    {
        auto data = DataType(boost::extents[cImage.height()][cImage.width()]);
        for (std::size_t y = 0; y < cImage.height(); ++y) {
            for (std::size_t x = 0; x < cImage.width(); ++x) {
                data(std::array<std::size_t,2>{{y,x}}) = cImage.at(y,x);
            }
        }
        return {std::move(data)};
    }

    std::size_t height() const;
    std::size_t width() const;

    Color& at(std::size_t y, std::size_t x);
    const Color& at(std::size_t y, std::size_t x) const;

    RowsSlicingIterator rows_begin();
    ConstRowsSlicingIterator rows_begin() const;
    ConstRowsSlicingIterator rows_cbegin() const;

    RowsSlicingIterator rows_end();
    ConstRowsSlicingIterator rows_end() const;
    ConstRowsSlicingIterator rows_cend() const;

    ColumnsSlicingIterator columns_begin();
    ConstColumnsSlicingIterator columns_begin() const;
    ConstColumnsSlicingIterator columns_cbegin() const;

    ColumnsSlicingIterator columns_end();
    ConstColumnsSlicingIterator columns_end() const;
    ConstColumnsSlicingIterator columns_cend() const;
};


} // namespace etam

#endif // ETAM_IMAGE_HPP
