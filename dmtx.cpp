#include "dmtx.hpp"

#include "color.hpp"


namespace etam {
namespace dmtx {


int Image::bytesPerPixel() const
{
    return ::dmtxImageGetProp(mSelf.get(), ::DmtxPropBytesPerPixel);
}

std::size_t Image::width() const
{
    return ::dmtxImageGetProp(mSelf.get(), ::DmtxPropWidth);
}

std::size_t Image::height() const
{
    return ::dmtxImageGetProp(mSelf.get(), ::DmtxPropHeight);
}

Color Image::at(std::size_t y, std::size_t x) const
{
    return mSelf->pxl[(y*width() + x) * bytesPerPixel()] == 0 ? Color::Black : Color::White;
}


Encode::Encode()
    : mSelf(::dmtxEncodeCreate(), [] (::DmtxEncode* enc) { ::dmtxEncodeDestroy(&enc); })
{}

void Encode::setMarginSize(std::size_t size)
{
    ::dmtxEncodeSetProp(mSelf.get(), ::DmtxPropMarginSize, size);
}

void Encode::setModuleSize(std::size_t size)
{
    ::dmtxEncodeSetProp(mSelf.get(), ::DmtxPropModuleSize, size);
}

void Encode::encodeDataMatrix(const std::string& str)
{
    auto inputString = reinterpret_cast<unsigned char*>(const_cast<char*>(str.c_str()));
    if (::dmtxEncodeDataMatrix(mSelf.get(), str.size(), inputString) == DmtxFail) {
        throw Error{"::dmtxEncodeDataMatrix failed"};
    }
}

Image Encode::image() const
{
    return {mSelf, mSelf->image};
}


Image encodeString(const std::string& str)
{
    auto encode = Encode{};
    encode.setMarginSize(0);
    encode.setModuleSize(1);
    encode.encodeDataMatrix(str);
    return encode.image();
}


} // namespace dmtx
} // namespace etam
