#ifndef ETAM_COLOR_HPP
#define ETAM_COLOR_HPP

namespace etam {


enum class Color {
    White,
    Black,
};


} // namespace etam

#endif // ETAM_COLOR_HPP
