#include "image.hpp"


namespace etam {


Image::Image(DataType data)
    : mData(std::move(data))
{}

std::size_t Image::height() const
{
    return mData.shape()[0];
}

std::size_t Image::width() const
{
    return mData.shape()[1];
}

Color& Image::at(std::size_t y, std::size_t x)
{
    return mData(std::array<std::size_t,2>{{y,x}});
}
const Color& Image::at(std::size_t y, std::size_t x) const
{
    return mData(std::array<std::size_t,2>{{y,x}});
}

Image::RowsSlicingIterator Image::rows_begin()
{
    return makeSlicingIterator<0>(mData, 0);
}
Image::ConstRowsSlicingIterator Image::rows_begin() const
{
    return makeConstSlicingIterator<0>(mData, 0);
}
Image::ConstRowsSlicingIterator Image::rows_cbegin() const
{
    return rows_begin();
}

Image::RowsSlicingIterator Image::rows_end()
{
    return makeSlicingIterator<0>(mData, height());
}
Image::ConstRowsSlicingIterator Image::rows_end() const
{
    return makeConstSlicingIterator<0>(mData, height());
}
Image::ConstRowsSlicingIterator Image::rows_cend() const
{
    return rows_end();
}

Image::ColumnsSlicingIterator Image::columns_begin()
{
    return makeSlicingIterator<1>(mData, 0);
}
Image::ConstColumnsSlicingIterator Image::columns_begin() const
{
    return makeConstSlicingIterator<1>(mData, 0);
}
Image::ConstColumnsSlicingIterator Image::columns_cbegin() const
{
    return columns_begin();
}

Image::ColumnsSlicingIterator Image::columns_end()
{
    return makeSlicingIterator<1>(mData, width());
}
Image::ConstColumnsSlicingIterator Image::columns_end() const
{
    return makeConstSlicingIterator<1>(mData, width());
}
Image::ConstColumnsSlicingIterator Image::columns_cend() const
{
    return columns_end();
}


} // namespace etam
