#include "latex.hpp"

#include <algorithm>

#include <boost/spirit/include/karma_char.hpp>
#include <boost/spirit/include/karma_format.hpp>
#include <boost/spirit/include/karma_int.hpp>
#include <boost/spirit/include/karma_list.hpp> // operator %
#include <boost/spirit/include/karma_sequence.hpp> // operator <<
#include <boost/spirit/include/karma_stream.hpp>

#include <range/v3/core.hpp>
#include <range/v3/algorithm/max.hpp>
#include <range/v3/to_container.hpp>
#include <range/v3/view/concat.hpp>
#include <range/v3/view/for_each.hpp>
#include <range/v3/view/reverse.hpp>
#include <range/v3/view/transform.hpp>

#include "nonogram.hpp"
#include "pbnsolve.hpp"

namespace karma = boost::spirit::karma;


namespace etam {


namespace {

const auto header = R"END(
\documentclass{article}
\usepackage[a3paper,margin=1cm]{geometry}

\usepackage{logicpuzzle}
\nonogramsetup{counterstyle=none,scale=0.38,fontsize=scriptsize}

\begin{document}
)END";

std::size_t extracells(const Nonogram& nonogram)
{
    return
        ranges::max(
            ranges::view::concat(nonogram.rowHints(), nonogram.columnHints())
            | ranges::view::transform([](const auto& hints) { return hints.size(); })
        );
}

struct RowGroup
{
    std::size_t start, length;

    RowGroup(std::size_t start_, std::size_t length_)
        : start{start_}
        , length{length_}
    {}
};

std::ostream& operator<<(std::ostream& ostream, const RowGroup rowGroup)
{
    return ostream << rowGroup.start << '/' << rowGroup.length;
}

template <typename Row>
std::vector<RowGroup> countRowGroups(const Row& row)
{
    auto result = std::vector<RowGroup>{};
    auto start = std::size_t{0};
    auto length = std::size_t{0};
    auto i = std::size_t{0}; // TODO: zip
    for (const Color pixel : row) {
        if (pixel == Color::Black) {
            if (length == 0) {
                start = i;
            }
            ++length;
        } else { // pixel == Color::While
            if (length > 0) {
                result.emplace_back(start+1, length);
            }
            length = 0;
        }
        ++i;
    }
    if (length > 0) {
        result.emplace_back(start+1, length);
    }
    return result;
}

std::ostream& operator<<(std::ostream& ostream, _ToLatex<Nonogram> _toLatex)
{
    using karma::format;
    using karma::int_;
    using karma::stream;
    using ranges::view::reverse;
    using ranges::view::transform;
    using ranges::to_;

    const auto& nonogram = _toLatex.v;
    const auto hintsGenerator = ('{' << (int_ % ',') << '}') % ',';

    ostream << "\\begin{nonogram}[rows=" << nonogram.goal().height() << ",columns=" << nonogram.goal().width() << ",extracells=" << extracells(nonogram) << "]\n"
            << "    \\nonogramV{" << format(hintsGenerator, nonogram.rowHints() | reverse | transform(reverse) | to_<Nonogram::Hints>() /*spirit cannot eat ranges*/) << "}\n"
            << "    \\nonogramH{" << format(hintsGenerator, nonogram.columnHints() | transform(reverse) | to_<Nonogram::Hints>() /*spirit cannot eat ranges*/) << "}\n"
            << "\\end{nonogram}\n"
            << "\\newline\n"
            << "\\begin{nonogram}[rows=" << nonogram.goal().height() << ",columns=" << nonogram.goal().width() << ",extracells=0]\n";
    {
        const auto& image = nonogram.goal();
        auto i = image.height(); // TODO: zip
        std::for_each(image.rows_begin(), image.rows_end(), [&](const auto rowView) mutable {
            ostream << "    \\nonogramrow{" << (i--) << "}{" << format(stream % ',', countRowGroups(rowView)) << "}\n";
        });
    }
    ostream << "\\end{nonogram}\n"
            << "\\newpage\n\n";
    return ostream;
}

const auto footer = R"END(
\end{document}
)END";

} // namespace


std::ostream& operator<<(std::ostream& ostream, _ToLatex<std::vector<Nonogram>> _toLatex)
{
    ostream << header;
    for (const auto& nonogram: _toLatex.v)
        ostream << toLatex(nonogram);
    ostream << footer;
    return ostream;
}


} // namespace etam
