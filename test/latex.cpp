#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include <vector>

#include "main.hpp"
#include "../latex.hpp"
#include "../nonogram.hpp"

using boost::test_tools::output_test_stream;


BOOST_AUTO_TEST_SUITE( latex )

BOOST_AUTO_TEST_CASE( output )
{
    using namespace etam::test;
    output_test_stream output("latex.txt");
    output << etam::toLatex(std::vector<etam::Nonogram>{getTestNonogram()});
    BOOST_TEST( output.match_pattern() );
}

BOOST_AUTO_TEST_SUITE_END() // latex
