#include <boost/test/unit_test.hpp>

#include <ostream>
#include <vector>

#include "main.hpp"


namespace boost {
namespace test_tools {
namespace tt_detail {

template <typename T>
struct print_log_value<std::vector<T>>
{
    void operator()(std::ostream& ostream, const std::vector<T>& v)
    {
        ostream << '{';
        for (const auto& i : v)
            ostream << i << ',';
        ostream << '}';
    }
};

} // namespace tt_detail
} // namespace test_tools
} // namespace boost


BOOST_AUTO_TEST_SUITE( nonogram )

BOOST_AUTO_TEST_CASE( hints )
{
    using namespace etam::test;
    const auto nonogram = getTestNonogram();
    const auto& rowHints = nonogram.rowHints();
    const auto& columnHints = nonogram.columnHints();
    const auto expectedRowHints = getExpectedRowHints();
    const auto expectedColumnHints = getExpectedColumnHints();

    BOOST_CHECK_EQUAL_COLLECTIONS(rowHints.begin(), rowHints.end(), expectedRowHints.begin(), expectedRowHints.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(columnHints.begin(), columnHints.end(), expectedColumnHints.begin(), expectedColumnHints.end());
}

BOOST_AUTO_TEST_SUITE_END() // nonogram
