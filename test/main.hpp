#ifndef ETAM_TEST_MAIN_HPP
#define ETAM_TEST_MAIN_HPP

#include "../nonogram.hpp"


namespace etam {
namespace test {

Nonogram getTestNonogram();
Nonogram::Hints getExpectedRowHints();
Nonogram::Hints getExpectedColumnHints();

} // namespace test
} // namespace etam

#endif // ETAM_TEST_MAIN_HPP
