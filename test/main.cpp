#define BOOST_TEST_MODULE text_to_qrcode_nonogram
#include <boost/test/unit_test.hpp>

#include "main.hpp"

#include <array>
#include <vector>

#include "../color.hpp"
#include "../image.hpp"

namespace etam {
namespace test {


namespace {

struct YX { std::size_t y, x; };

template <std::size_t Width, std::size_t Height>
class RawImage
{
private:
    std::array<etam::Color,Width*Height> mData;

public:
    explicit
    RawImage(const std::vector<YX>& data)
    {
        mData.fill(etam::Color::White);
        for (const auto& xy : data)
            mData[xy.y*Width + xy.x] = etam::Color::Black;
    }

    auto width() const { return Width; }
    auto height() const { return Height; }
    auto at(const std::size_t y, const std::size_t x) const { return mData[y*Width+x]; }
};

} // namespace


Nonogram getTestNonogram()
{
    return Nonogram{Image::fromCImage(RawImage<10,10>{std::vector<YX>{
              {0,1},{0,2},{0,3},                  {0,7},{0,8},{0,9},
        {1,0},{1,1},                        {1,6},{1,7},      {1,9},
        {2,0},                              {2,6},{2,7},{2,8},{2,9},
        {3,0},                                    {3,7},{3,8},{3,9},
        {4,0},      {4,2},{4,3},                  {4,7},{4,8},{4,9},
        {5,0},{5,1},{5,2},{5,3},{5,4},                  {5,8},{5,9},
        {6,0},{6,1},{6,2},{6,3},{6,4},{6,5},{6,6},{6,7},{6,8},{6,9},
              {7,1},{7,2},{7,3},{7,4},{7,5},{7,6},{7,7},{7,8},{7,9},
                          {8,3},{8,4},{8,5},{8,6},{8,7},{8,8},{8,9},
                                      {9,5},{9,6},{9,7},{9,8},{9,9},
    }})};
}

Nonogram::Hints getExpectedRowHints()
{
    return {{3,3},{2,2,1},{1,4},{1,3},{1,2,3},{5,2},{10},{9},{7},{5}};
}

Nonogram::Hints getExpectedColumnHints()
{
    return {{6},{2,3},{1,4},{1,5},{4},{4},{2,4},{5,4},{1,8},{10}};
}


} // namespace test
} // namespace etam
