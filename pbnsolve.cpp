#include "pbnsolve.hpp"

#include <algorithm>
#include <ostream>
#include <string>
#include <vector>

#include <Poco/Process.h>
#include <Poco/PipeStream.h>
#include <Poco/StreamCopier.h>

#include "color.hpp"
#include "image.hpp"


namespace etam {
namespace pbnsolve {


namespace {

struct _InputXML
{
    const Image& image;
};

_InputXML inputXML(const Image& image)
{
    return {image};
}

std::ostream& operator<<(std::ostream& out, _InputXML _inputXML)
{
    const Image& image = _inputXML.image;
    out << "<?xml version=\"1.0\"?>\n"
           "<!DOCTYPE pbn SYSTEM \"http://webpbn.com/pbn-0.3.dtd\">\n"
           "<puzzleset>\n"
           "<puzzle type=\"grid\" defaultcolor=\"black\">\n"
           "<color name=\"white\" char=\".\">fff</color>\n"
           "<color name=\"black\" char=\"X\">000</color>\n"
           "<solution type=\"goal\">\n"
           "<image>\n";
    std::for_each(image.rows_begin(), image.rows_end(), [&](auto rowView) mutable {
        out << '|';
        for (Color color : rowView) {
            out << (color == Color::Black ? 'X' : '.');
        }
        out << "|\n";
    });
    out << "</image>\n"
           "</solution>\n"
           "</puzzle>\n"
           "</puzzleset>\n";
    return out;
}

bool isSubString(const std::string& string, const std::string& subString)
{
    return std::search(string.begin(), string.end(), subString.begin(), subString.end()) != string.end();
}

Result makeResult(const std::string& out)
{
    if (isSubString(out, "unique")) {
        if (isSubString(out, "logical")) {
            return Result::UniqueLogical;
        }
        return Result::Unique;
    } else if (isSubString(out, "multiple")) {
        return Result::Multiple;
    } else if (isSubString(out, "timeout")) {
        return Result::Timeout;
    }
    return Result::Unknown;
}

std::string suckStream(std::istream& istream)
{
    std::ostringstream osstream;
    Poco::StreamCopier::copyStream(istream, osstream);
    return osstream.str();
}

std::string suckStream(std::istream&& istream)
{
    return suckStream(istream);
}

} // namespace


Result checkUnique(const Image& image)
{
    Poco::Pipe outPipe, inPipe;
    Poco::ProcessHandle ph = Poco::Process::launch("pbnsolve", std::vector<std::string>{"-ub", "-x10"}, &outPipe, &inPipe, nullptr);

    Poco::PipeOutputStream(outPipe) << inputXML(image);
    outPipe.close(Poco::Pipe::CLOSE_WRITE);

    const auto result = makeResult(suckStream(Poco::PipeInputStream(inPipe)));
    ph.wait();
    return result;
}


} // namespace pbnsolve
} // namespace etam
