Convert text to [QR code](https://en.wikipedia.org/wiki/QR_code) and [Data Matrix](https://en.wikipedia.org/wiki/Data_Matrix) as solvable [nonogram](https://en.wikipedia.org/wiki/Nonogram)
============================================================================================================================================================================================

Usage
-----

    ./main "text" > text.tex
    pdflatex text.tex

Requirements
------------

  * C++ compiler with C++14 support (tested with GCC 5.3)
  * Boost (tested with 1.60.0)
  * [POCO](http://pocoproject.org/) (tested with 1.7.2)
  * [libqrencode](https://fukuchi.org/works/qrencode/) (tested with 3.4.3)
  * [libdmtx](http://libdmtx.sourceforge.net/) (tested with 0.7.4)
  * [etam/boost_multiarray_slicing_iterator](https://github.com/etam/boost_multiarray_slicing_iterator) (tested with git 03781cd)
  * [ericniebler/range-v3](https://github.com/ericniebler/range-v3) (tested with git fe27e87)
  * [beark/ftl](https://github.com/beark/ftl) (tested with git 707f513)
  * [pbnsolve](http://webpbn.com/pbnsolve.html) (tested with 1.10)
  * [latex logicpuzzle](https://www.ctan.org/tex-archive/graphics/pgf/contrib/logicpuzzle) (at least 2.5)
