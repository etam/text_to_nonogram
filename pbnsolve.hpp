#ifndef ETAM_PBNSOLVE_HPP
#define ETAM_PBNSOLVE_HPP

#include <string>


namespace etam {

class Image;


namespace pbnsolve {


enum class Result
{
    Unknown,
    UniqueLogical,
    Unique,
    Multiple,
    Timeout,
};

Result checkUnique(const Image& image);


} // namespace pbnsolve
} // namespace etam

#endif // ETAM_PBNSOLVE_HPP
