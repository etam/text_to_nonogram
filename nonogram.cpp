#include "nonogram.hpp"

#include <algorithm>
#include <iterator>

#include "image.hpp"
#include "pbnsolve.hpp"


namespace etam {


namespace {

template <typename It1, typename It2, typename T>
void countSeriesOfValue(It1 first, const It1 last, It2 outFirst, const T& value)
{
    auto count = std::size_t{0};
    for (; first != last; ++first) {
        if (*first == value) {
            ++count;
        } else if (count > 0) {
            *(outFirst++) = count;
            count = 0;
        }
    }
    if (count > 0)
        *(outFirst++) = count;
}

template <typename LineView>
std::vector<std::size_t> createHintsLine(const LineView& lineView)
{
    auto result = std::vector<std::size_t>{};
    countSeriesOfValue(lineView.begin(), lineView.end(), std::back_inserter(result), Color::Black);
    return result;
}

template <typename It>
std::vector<std::vector<std::size_t>> createHints(It first, It last)
{
    auto result = std::vector<std::vector<std::size_t>>{};
    result.reserve(last - first);
    std::transform(first, last, std::back_inserter(result), [](const auto& lineView) {
        return createHintsLine(lineView);
    });
    return result;
}

} // namespace


Nonogram::Nonogram(Image image)
    : mGoal{std::move(image)}
    , mRowHints{createHints(mGoal.rows_begin(), mGoal.rows_end())}
    , mColumnHints{createHints(mGoal.columns_begin(), mGoal.columns_end())}
    , mSolvability{pbnsolve::checkUnique(image)}
{}

const Image& Nonogram::goal() const
{
    return mGoal;
}

const Nonogram::Hints& Nonogram::rowHints() const
{
    return mRowHints;
}

const Nonogram::Hints& Nonogram::columnHints() const
{
    return mColumnHints;
}

pbnsolve::Result Nonogram::solvability() const
{
    return mSolvability;
}

bool Nonogram::isSolvable() const
{
    return mSolvability == pbnsolve::Result::UniqueLogical;
}

} // namespace etam
