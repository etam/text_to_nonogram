#ifndef ETAM_DMTX_HPP
#define ETAM_DMTX_HPP

#include <memory>
#include <stdexcept>
#include <string>

#include <dmtx.h>


namespace etam {

enum class Color;


namespace dmtx {


class Error
    : std::runtime_error
{
private:
    typedef std::runtime_error ParentType;
public:
    using ParentType::ParentType;
};


class Image
{
private:
    friend class Encode;

    std::shared_ptr<::DmtxImage> mSelf;

    template <typename Y>
    Image(const std::shared_ptr<Y>& parent, ::DmtxImage* self)
        : mSelf(parent, self)
    {}

    int bytesPerPixel() const;

public:
    std::size_t width() const;
    std::size_t height() const;
    Color at(std::size_t y, std::size_t x) const;
};


class Encode
{
private:
    std::shared_ptr<::DmtxEncode> mSelf;

public:
    Encode();
    void setMarginSize(std::size_t size);
    void setModuleSize(std::size_t size);
    void encodeDataMatrix(const std::string& str);
    Image image() const;
};


Image encodeString(const std::string& str);


} // namespace dmtx
} // namespace etam

#endif // ETAM_DMTX_HPP
