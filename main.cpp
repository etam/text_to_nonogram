#include <iostream>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <ftl/maybe.h> // TODO: use std(::experimental)::optional
#include <ftl/vector.h>

#include <range/v3/core.hpp>
#include <range/v3/action/push_back.hpp>
#include <range/v3/view/transform.hpp>

#include "dmtx.hpp"
#include "image.hpp"
#include "latex.hpp"
#include "nonogram.hpp"
#include "pbnsolve.hpp"
#include "qrcode.hpp"

using namespace etam;


namespace {

namespace func {

template <
    typename Maybe,
    typename = std::enable_if_t<
        std::is_same<
            std::decay_t<Maybe>,
            ftl::maybe<ftl::Value_type<std::decay_t<Maybe>>>
        >::value
    >
>
std::vector<ftl::Value_type<std::decay_t<Maybe>>> toVector(Maybe&& maybe)
{
    typedef ftl::Value_type<std::decay_t<Maybe>> value_type;
    auto result = std::vector<value_type>{};
    maybe.matchE(
        [&result](value_type& value) mutable { result.push_back(std::move(value)); },
        [](ftl::Nothing) {}
    );
    return result;
}

ftl::maybe<dmtx::Image> dmtxEncodeString(const std::string& string)
{
    try {
        return ftl::just(dmtx::encodeString(string));
    } catch (dmtx::Error&) {
        return ftl::Nothing{};
    }
}

ftl::maybe<Image> ImageFromCImage(const dmtx::Image& cImage)
{
    return ftl::just(Image::fromCImage(cImage));
}

} // namespace func

std::vector<Image> getImages(const std::string& string)
{
    using ftl::operator<<=;

    auto result = [&]() -> std::vector<Image> {
        const auto qrcodeRawImages = qrcode::encodeStringAllVariants(string);
        return qrcodeRawImages | ranges::view::transform(&Image::fromCImage<qrcode::Image>);
    }();

    ranges::action::push_back(
        result,
        func::toVector(func::ImageFromCImage <<= func::dmtxEncodeString(string))
    );

    return result;
}

std::vector<Nonogram> getSolvableNonograms(const std::string& string)
{
    auto images = getImages(string);
    // TODO: ranges?
    auto nonograms = std::vector<Nonogram>{};
    for (auto& image : images) {
        auto nonogram = Nonogram{std::move(image)};
        if (nonogram.isSolvable())
            nonograms.push_back(std::move(nonogram));
    }
    return nonograms;
}

} // namespace


int main(int argc, char* argv[])
{
    if (argc != 2)
        return 1;

    std::cout << etam::toLatex(getSolvableNonograms(argv[1]));
}
